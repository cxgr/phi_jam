﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class RoomNode : MonoBehaviour
	{
		public RoomPortal[] portals;
		
		RoomObject roomObj;
		public RoomObject RoomObj
		{
			get
			{
				if (!roomObj)
					roomObj = GetComponent<RoomObject>();
				return roomObj;
			}
		}
	}
}
