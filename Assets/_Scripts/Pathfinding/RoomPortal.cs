﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class RoomPortal : MonoBehaviour
	{
		public bool isBackwallPortal;
		
		public RoomNode ownerRoom;
		
		[Header("assign manually")]
		public RoomPortal connectedPortal;
		
		void OnDrawGizmos()
		{
			if (null != ownerRoom)
			{
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(transform.position, ownerRoom.transform.position);
			}
			else
			{
				Gizmos.color = Color.yellow;
				Gizmos.DrawWireSphere(transform.position, .3f);
			}
			
			if (null != connectedPortal)
			{
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(transform.position, connectedPortal.transform.position);
			}
			else
			{
				Gizmos.color = Color.red;
				Gizmos.DrawSphere(transform.position, .25f);
			}
		}
			
	}
}
