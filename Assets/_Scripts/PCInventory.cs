﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class PCInventory : MonoBehaviour
	{
		public InventoryItem heldItem;
		
		public Transform itemSlot;
		
		public void HoldItem(InventoryItem item)
		{
			item.ToggleClickability(false);
			heldItem = item;
			heldItem.transform.parent = itemSlot;
			heldItem.transform.localPosition = Vector3.zero;
			heldItem.transform.localRotation = Quaternion.identity;
		}
		
		public void LeaveItem(Transform tRef)
		{
			if (null == heldItem) return;
			heldItem.transform.parent = GetComponent<PCController>().currentRoom.transform;
			heldItem.transform.position = tRef.position;
			heldItem.transform.rotation = tRef.rotation;
			heldItem.ToggleClickability(true);
			heldItem = null;
		}
	}
}
