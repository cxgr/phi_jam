﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class GameController : MonoBehaviour
	{
		public static GameController Instance { get { return SingletonUtils<GameController>.Instance; } }
		
		public RoomObject currentRoom;
		
		public bool IsCurrentRoom(RoomObject ro)
		{
			return null != currentRoom && ro.GetInstanceID() == currentRoom.GetInstanceID();
		}
	}
}
