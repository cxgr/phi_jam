﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
    public enum EEmotes
    {
        Pos,
        Neg,
    }
    
    [System.Serializable]
    public struct KeySpritePair
    {
        public EEmotes key;
        public Sprite spr;
    }

	public class PCEMotes : MonoBehaviour
	{
        public KeySpritePair[] keySpritePairs;
        Dictionary<EEmotes, Sprite> emotesDict = new Dictionary<EEmotes, Sprite>();

        public GameObject bubbleRootObj;
        public SpriteRenderer emoteSR;

        public InputManager im;

        float visibilityTime;

        private void Awake()
        {
            foreach (var ksp in keySpritePairs)
                emotesDict.Add(ksp.key, ksp.spr);
            bubbleRootObj.SetActive(false);
        }

        public void ShowEmote(EEmotes eType, float time = 1f)
        {
            visibilityTime = time;
            bubbleRootObj.SetActive(true);
            emoteSR.sprite = emotesDict[eType];
        }

        private void Update()
        {
            if (bubbleRootObj.activeSelf)
            {
                visibilityTime -= Time.deltaTime;
                if (visibilityTime <= 0f)
                    bubbleRootObj.SetActive(false);
            }
        }
    }
}