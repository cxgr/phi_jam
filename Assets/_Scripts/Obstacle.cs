﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class Obstacle : MonoBehaviour
	{
		[Header("run away to point within X bounds of the same room")]
		public Transform runAwayPoint;
		[Header("run away to centre (root transform) of specified room node")]
		public RoomNode runAwayRoom;
        public InventoryItem runAwayItem;
        public InteractionTarget runAwayInteraction;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            if (null != runAwayPoint)
            {
                Gizmos.DrawSphere(runAwayPoint.transform.position, .35f);
                Gizmos.DrawLine(transform.position, runAwayPoint.transform.position);
            }
        }
    }
}
