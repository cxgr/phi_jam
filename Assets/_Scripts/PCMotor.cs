﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Phijam
{
	public class PCMotor : MonoBehaviour
	{
		public float xDir;
		public float zDir;
		public float walkSpd;
		
		public Transform tAnim;
		public Animator anim;
		
		public Action<Obstacle> EncounteredObstacleEvent;
		
		public void MotorUpdate()
		{
			if (Mathf.Abs(xDir) > 0f)
			{
				tAnim.transform.localRotation = Quaternion.Euler(Vector3.up * (xDir > 0 ? 90f : 270f));
				transform.Translate(Vector3.right * xDir * walkSpd * Time.deltaTime, Space.World);
			}
			else if (Mathf.Abs(zDir) > 0f)
			{
				tAnim.transform.localRotation = Quaternion.Euler(Vector3.up * (zDir > 0 ? 0f : 180f));
			}
			
			anim.SetFloat("walk", Mathf.Max(Mathf.Abs(xDir), Mathf.Abs(zDir)));
		}
		
		void OnTriggerEnter(Collider other)
		{
			Debug.LogWarning("enter " + other.name);
			var obstacle = other.GetComponent<Obstacle>();
			if (null != obstacle && null != EncounteredObstacleEvent)
				EncounteredObstacleEvent(obstacle);
		}
	}
}
