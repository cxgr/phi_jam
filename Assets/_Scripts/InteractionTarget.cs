﻿using UnityEngine;
using System.Linq;
using UnityEngine.Events;

namespace Phijam
{
    [System.Serializable]
    public class InteractionEvent : UnityEvent
    {

    }
	public class InteractionTarget : MonoBehaviour
	{
        public bool requireItem;
        public InventoryItem.ItemKeys requiredIKey;

        public InteractionEvent onCompleteCallback;

        void Awake()
        {
            if (null == GetHostRoom())
            {
                Debug.LogError(name + " item wasnt parented properly at startup");
                transform.SetParent(FindObjectsOfType<RoomNode>()
                    .OrderBy(r => Vector3.Distance(transform.position, r.transform.position))
                    .FirstOrDefault().transform, true);
            }
            Debug.Log(GetHostRoom());
        }

        public RoomNode GetHostRoom()
        {
            return GetComponentInParent<RoomNode>();
        }
    }
}
