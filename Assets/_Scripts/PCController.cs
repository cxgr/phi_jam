﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Phijam
{
	public class PCController : MonoBehaviour
	{
		public static PCController Instance { get { return SingletonUtils<PCController>.Instance; } }
		
		public InputManager im;
		public Pathfinding pf;
		
		public PCMotor motor;
		public PCInventory inventory;
        public PCEMotes emotes;
		
		public RoomNode currentRoom;
		
		public Transform pathMarker;
		
		Queue<PCAction> actionQ = new Queue<PCAction>();
		
		public bool ShouldIgnoreInput()
		{
			return actionQ.Count > 0 && !IsCancellableAction(actionQ.Peek());
		}
		
		void Awake()
		{
            im.SelfClickEvent += SelfClickHandler;
			im.WalkToEvent += WalkToHandler;
			im.PickItemEvent += PickItemHandler;
            im.InteractionEvent += InteractionHandler;
			motor.EncounteredObstacleEvent += ObstacleEncounterHandler;
		}

        public void SelfClickHandler()
        {
            //TODO give walkthrough hints
            if (actionQ.Count <= 0)
                emotes.ShowEmote(EEmotes.Pos, 1f);
        }
		
		public void WalkToHandler(RoomNode rn, Vector3 targetPos, bool uninteruptableWalkto = false)
		{	
			Debug.Log("walk to: " + rn.RoomObj.roomKey);
			var walkToPos = targetPos;
			walkToPos.y = rn.transform.position.y;
			pathMarker.transform.position = walkToPos;
			
			var finalRoomAfterCancel = AbortQ();
			Debug.Log("after cancel: " + finalRoomAfterCancel.RoomObj.roomKey);
			
			if (rn == finalRoomAfterCancel)
				actionQ.Enqueue(new SimpleMove(walkToPos.x, !uninteruptableWalkto));
			else
			{
				var path = pf.GetPath(finalRoomAfterCancel, rn);
				if (path.Count <= 0)
				{
					Debug.LogWarning("no path, idling");
					return;
				}
			
				for (int i = 0; i < path.Count; ++i)
				{
					var fromRoom = (i > 0) ? path[i - 1] : finalRoomAfterCancel;
					var toRoom = path[i];
					var targetPortal = fromRoom.portals.Where(p => p.connectedPortal.ownerRoom == toRoom).FirstOrDefault();
					actionQ.Enqueue(new SimpleMove(targetPortal.transform.position.x, !uninteruptableWalkto));
					if (targetPortal.isBackwallPortal)
						actionQ.Enqueue(new GoThroughBackwallPortal(targetPortal));
					else
						actionQ.Enqueue(new GoThroughPortal(targetPortal));
				}
				actionQ.Enqueue(new SimpleMove(walkToPos.x));
			}
			if (currentRoom == finalRoomAfterCancel)
				RunQ();
		}
		
		public void PickItemHandler(InventoryItem item)
		{
			AbortQ();
			if (actionQ.Count > 0)
				return;

			var hostRoom = item.GetHostRoom();
			if (hostRoom == currentRoom || pf.GetPath(currentRoom, hostRoom).Count > 0)
			{
				WalkToHandler(hostRoom, item.transform.position);
				actionQ.Enqueue(new PickOrSwapItem(item));
			}
		}

        public void InteractionHandler(InteractionTarget interactive)
        {
            AbortQ();
            if (actionQ.Count > 0)
                return;

            var hostRoom = interactive.GetHostRoom();
            if (hostRoom == currentRoom || pf.GetPath(currentRoom, hostRoom).Count > 0)
            {
                WalkToHandler(hostRoom, interactive.transform.position);
                actionQ.Enqueue(new InteractWith(interactive));
            }
        }
		
		public void ObstacleEncounterHandler(Obstacle o)
		{
			if (!o) return;
			
			AbortQ();
			actionQ.Enqueue(new ObstaclePanic(o, null));
			
			if (null != o.runAwayPoint)
				WalkToHandler(currentRoom, o.runAwayPoint.transform.position, true);
			else if (null != o.runAwayRoom)
			{
				WalkToHandler(o.runAwayRoom, o.runAwayRoom.transform.position, true);
				RunQ();
			}
            else if (null != o.runAwayItem)
            {
                WalkToHandler(o.runAwayItem.GetHostRoom(), o.runAwayItem.transform.position, true);
                actionQ.Enqueue(new PickOrSwapItem(o.runAwayItem));
                RunQ();
            }
            else if (null != o.runAwayInteraction)
            {
                if (o.runAwayInteraction.requireItem)
                    Debug.LogError(o.runAwayInteraction + " requires item");
                WalkToHandler(o.runAwayInteraction.GetHostRoom(), o.runAwayInteraction.transform.position, true);
                actionQ.Enqueue(new InteractWith(o.runAwayInteraction));
                RunQ();
            }
			else
				Debug.LogError("no runaway dest on obstacle " + o.name);
		}
		
		void RunQ()
		{
			if (actionQ.Count > 0)
				actionQ.Peek().StartAction();
		}
		
		RoomNode AbortQ()
		{
			var finalRoom = currentRoom;
			if (actionQ.Count <= 0f) return finalRoom;
			
			if (IsCancellableAction(actionQ.Peek()))
			{
				actionQ.Dequeue().FinishAction();
				actionQ.Clear();
			}
			else
			{
				var tmp = new Queue<PCAction>();
				while (actionQ.Count > 0 && !IsCancellableAction(actionQ.Peek()))
				{
					var actionToFinish = actionQ.Dequeue();
					tmp.Enqueue(actionToFinish);
					if (actionToFinish is IRoomTransferAction)
						finalRoom = (actionToFinish as IRoomTransferAction).GetFinalRoom();
				}
				actionQ = tmp;
			}
			return finalRoom;
		}
		
		bool IsCancellableAction(PCAction pca)
		{
			return pca is SimpleMove && (pca as SimpleMove).isInterruptable;
		}
		
		void Update()
		{
			if (actionQ.Count > 0)
			{
				if (actionQ.Peek().IsComplete())
				{
					actionQ.Dequeue().FinishAction();
					RunQ();
				}
				else
					actionQ.Peek().ActionUpdate();
			}
			
			motor.MotorUpdate();
		}
	}
}